import 'dotenv/config';
import { AppDataSource } from '../src/configs/database/data-source';
import { Post } from '../src/posts/entities/post.entity';
import request from 'supertest';
import app from '../src/application/app';
import { DataSource } from 'typeorm';

let dataSource: DataSource;

const postRepository = AppDataSource.getRepository(Post);

beforeAll(async () => {
  dataSource = await AppDataSource.initialize();
});

beforeEach(async () => {
  await postRepository.clear();
});

afterAll(async () => {
  await dataSource.destroy();
});

describe('/posts', () => {
  describe('POST /posts', () => {
    it('returns 400 status if title is not passed', async () => {
      await request(app)
        .post('/api/v1/posts')
        .send({ description: 'hello' })
        .expect(400);
    });

    it('returns 400 status if content is not passed', async () => {
      await request(app)
        .post('/api/v1/posts')
        .send({ title: 'hello' })
        .expect(400);
    });

    it('saves post', async () => {
      const title = 'New Post';
      const description = 'Content Post with info';

      await request(app)
        .post('/api/v1/posts')
        .send({ title, description })
        .expect(201);

      const posts = await postRepository.find();

      expect(posts.length).toEqual(1);

      expect(posts[0]).toMatchObject({
        title,
        description,
      });
    });

    it('returns created post', async () => {
      const response = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post', description: 'Content Post with info' })
        .expect(201);

      const posts = await postRepository.find();

      expect(posts.length).toEqual(1);

      const { id, title, description, createdAt, updatedAt } = posts[0];

      expect(response.body).toEqual({
        id,
        title,
        description,
        createdAt: createdAt.toISOString(),
        updatedAt: updatedAt.toISOString(),
      });
    });
  });

  describe('GET /posts', () => {
    it('returns an empty list if no posts', async () => {
      const response = await request(app).get('/api/v1/posts').expect(200);

      expect(response.body).toEqual([]);
    });
    it('returns all saved posts', async () => {
      const { body: firstPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post', description: 'Content Post with info' })
        .expect(201);

      const { body: secondPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post 2', description: 'Some another info' })
        .expect(201);

      const response = await request(app).get('/api/v1/posts').expect(200);

      expect(response.body).toEqual([secondPost, firstPost]);
      expect(response.body.length).toEqual(2);
      expect(response.body[0]).toHaveProperty('id');
      expect(response.body[0]).toHaveProperty('title', 'New Post 2');
      expect(response.body[0]).toHaveProperty(
        'description',
        'Some another info',
      );
      expect(response.body[0]).toHaveProperty('createdAt');
      expect(response.body[0]).toHaveProperty('updatedAt');
    });
    it('returns all posts sorted by createdAt in descending order', async () => {
      const post1 = await new Promise((resolve) => {
        setTimeout(async () => {
          const { body } = await request(app)
            .post('/api/v1/posts')
            .send({
              title: 'New Post 1',
              description: 'Content Post with info',
            })
            .expect(201);
          resolve(body);
        }, 1000);
      });

      const post2 = await new Promise((resolve) => {
        setTimeout(async () => {
          const { body } = await request(app)
            .post('/api/v1/posts')
            .send({ title: 'New Post 2', description: 'Some another info' })
            .expect(201);
          resolve(body);
        }, 2000);
      });

      const post3 = await new Promise((resolve) => {
        setTimeout(async () => {
          const { body } = await request(app)
            .post('/api/v1/posts')
            .send({ title: 'New Post 3', description: 'Some more info' })
            .expect(201);
          resolve(body);
        }, 3000);
      });

      const response = await request(app).get('/api/v1/posts').expect(200);

      const posts = response.body;

      expect(posts).toHaveLength(3);
      expect(posts[0].title).toEqual('New Post 3');
      expect(posts[1].title).toEqual('New Post 2');
      expect(posts[2].title).toEqual('New Post 1');
    });
    it('returns the post with the specified ID', async () => {
      const { body: createdPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post', description: 'Content Post with info' })
        .expect(201);

      const { id } = createdPost;

      const response = await request(app)
        .get(`/api/v1/posts/${id}`)
        .expect(200);

      expect(response.body).toEqual(createdPost);
    });
    it('returns 404 if post doesnt exist in database', async () => {
      await request(app).get('/api/v1/posts/999').expect(404);
    });
  });

  describe('PATCH /posts', () => {
    it('returns 404 if post doesnt exist in database', async () => {
      await request(app)
        .patch(`/api/v1/posts/999`)
        .send({ title: 'changed New Post 1' })
        .expect(404);
    });
    it('returns 400 status if invalid body is passed', async () => {
      const { body: testPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post', description: 'New Description' })
        .expect(201);

      const postId = testPost.id;

      await request(app)
        .patch(`/api/v1/posts/${postId}`)
        .send({ invalidField: 'Invalid Value' })
        .expect(400);
    });
    it('updates post by id', async () => {
      const createResponse = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'New Post', description: 'New Description' })
        .expect(201);

      const postId = createResponse.body.id;

      const updatedPostData = {
        title: 'Updated Title',
        description: 'Updated Description',
      };

      await request(app)
        .patch(`/api/v1/posts/${postId}`)
        .send(updatedPostData)
        .expect(204);

      const postResponse = await request(app)
        .get(`/api/v1/posts/${postId}`)
        .expect(200);

      expect(postResponse.body).toMatchObject({
        title: updatedPostData.title,
        description: updatedPostData.description,
      });
    });
  });

  describe('DELETE /posts', () => {
    it('returns 404 if post doesnt exist in database', async () => {
      await request(app).delete(`/api/v1/posts/999`).expect(404);
    });

    it('deletes post', async () => {
      const { body: testPost } = await request(app)
        .post('/api/v1/posts')
        .send({ title: 'Post to be deleted', description: 'Some another info' })
        .expect(201);

      const response = await request(app).get('/api/v1/posts').expect(200);

      const postId = response.body[0].id;

      await request(app).delete(`/api/v1/posts/${postId}`).expect(204);

      const responseAfterDelete = await request(app)
        .get('/api/v1/posts')
        .expect(200);

      expect(responseAfterDelete.body).toEqual([]);
    });
  });
});
