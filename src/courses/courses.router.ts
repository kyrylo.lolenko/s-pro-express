import { Router } from 'express';
import * as coursesController from './courses.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import {
  addLectorToCourseIdParamSchema,
  courseCreateSchema,
  courseUpdateSchema,
} from './course.schema';
import { idParamSchema } from '../application/schemas/id-param.schema';

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses:
 *   get:
 *     summary: Get all courses
 *     tags: [Courses]
 *     responses:
 *       200:
 *         description: The list of courses.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Course'
 *       500:
 *         description: Some server error
 *
 */

router.get('/', controllerWrapper(coursesController.getAllCourses));

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses/{id}:
 *   get:
 *     summary: Get course by id
 *     tags: [Courses]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *          type: string
 *         required: true
 *         description: Course id
 *     responses:
 *       200:
 *         description: The course by id.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Course'
 *       404:
 *         description: The course was not found
 *       500:
 *         description: Some server error
 *
 */

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(coursesController.getCourseById),
);

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses/{id}/marks:
 *   get:
 *     summary: Get selected by id course's marks
 *     tags: [Courses]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *          type: string
 *         required: true
 *         description: Course id
 *     responses:
 *       200:
 *         description: Selected by id course's marks .
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/CourseMarks'
 *       404:
 *         description: The course was not found
 *       500:
 *         description: Some server error
 *
 */

router.get(
  '/:id/marks',
  validator.params(idParamSchema),
  controllerWrapper(coursesController.getMarksForCourse),
);

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses:
 *   post:
 *     summary: Create course
 *     tags: [Courses]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/CourseCreate'
 *     responses:
 *       200:
 *         description: Return the course after creation.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Course'
 *       400:
 *         description: Validation failed
 *       500:
 *         description: Some server error
 *
 */

router.post(
  '/',
  validator.body(courseCreateSchema),
  controllerWrapper(coursesController.createCourse),
);

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses/{id}:
 *   patch:
 *     summary: Update course by id
 *     tags: [Courses]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Course id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/CourseUpdate'
 *     responses:
 *       204:
 *         description: Course was updated
 *       404:
 *         description: Course was not found
 *       500:
 *         description: Some server error
 */

router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(courseUpdateSchema),
  controllerWrapper(coursesController.updateCourseById),
);

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses/{courseId}/lector/{lectorId}:
 *   patch:
 *     summary: Add lector to a course
 *     tags: [Courses]
 *     parameters:
 *       - name: courseId
 *         in: path
 *         description: ID of the course to update
 *         required: true
 *         schema:
 *           type: string
 *       - name: lectorId
 *         in: path
 *         description: ID of the lector to add
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       204:
 *         description: Course was updated
 *       404:
 *         description: Course was not found
 *       500:
 *         description: Some server error
 */

router.patch(
  '/:courseId/lector/:lectorId',
  validator.params(addLectorToCourseIdParamSchema),
  controllerWrapper(coursesController.addLectorToCourse),
);

/**
 * @swagger
 * tags:
 *   name: Courses
 *   description: The courses managing API
 * /courses/{id}:
 *   delete:
 *     summary: Delete course
 *     tags: [Courses]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The course id
 *     responses:
 *       200:
 *         description: The course was deleted
 *       404:
 *         description: The course was not found
 *       500:
 *         description: Some server error
 *
 */

router.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(coursesController.deleteCourseById),
);

export default router;
