import Joi from 'joi';
import { ICourse } from './types/course.interface';

export const courseCreateSchema = Joi.object<Omit<ICourse, 'id'>>({
  name: Joi.string().required(),
  description: Joi.string().required(),
  hours: Joi.number().required(),
});

export const courseUpdateSchema = Joi.object<Partial<ICourse>>({
  name: Joi.string().optional(),
  description: Joi.string().optional(),
  hours: Joi.number().optional(),
});

export const addLectorToCourseIdParamSchema = Joi.object<{
  courseId: string;
  lectorId: string;
}>({
  courseId: Joi.string().required(),
  lectorId: Joi.string().required(),
});
