import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Course } from '../entities/course.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     CourseCreate:
 *       type: object
 *       required:
 *         - name
 *         - description
 *         - hours
 *       properties:
 *         name:
 *           type: string
 *           description: name of course
 *         description:
 *           type: string
 *           description: description of course
 *         hours:
 *           type: number
 *           description: amount of hours dedicated for this course
 *       example:
 *         name: Math
 *         description: This course will cover basics of Math
 *         hours: 40
 */

export interface ICourseCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Course, 'id'>;
}

export interface IAddLectorToCourseRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Pick<Course, 'lectors'>;
}
