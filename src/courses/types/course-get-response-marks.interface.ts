/**
 * @swagger
 * components:
 *   schemas:
 *     CourseMarks:
 *       type: object
 *       required:
 *         - courseName
 *         - lectorName
 *         - studentName
 *         - mark
 *       properties:
 *         courseName:
 *           type: string
 *           description: Name of the course
 *         lectorName:
 *           type: string
 *           description: Name of the lector
 *         studentName:
 *           type: string
 *           description: Name of the student
 *         mark:
 *           type: string
 *           description: Mark for this course
 *       example:
 *         courseName: 'Programming'
 *         lectorName: 'Viktor'
 *         studentName: 'Taras'
 *         mark: 10
 */
