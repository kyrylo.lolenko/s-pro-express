import { ICourse } from './course.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     Course:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - description
 *         - hours
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the course
 *         createdAt:
 *           type: string
 *           format: date
 *           description: created at time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updated at time
 *         name:
 *           type: string
 *           description: name of course
 *         description:
 *           type: string
 *           description: description of course
 *         hours:
 *           type: number
 *           description: amount of hours dedicated for this course
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Math
 *         description: This course will cover basics of Math
 *         hours: 40
 */

export interface ICourseGetResponse extends ICourse {
  createdAt: Date;
  updatedAt: Date;
}
