import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ICourse } from './course.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     CourseUpdate:
 *       type: object
 *       properties:
 *        name:
 *          type: string
 *        description:
 *          type: string
 *        hours:
 *          type: number
 *       example:
 *         name: Programmin
 *         description: This course will cover basics of programming
 *         hours: 50
 */

export interface ICourseUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<ICourse>;
}
