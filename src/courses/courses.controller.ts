import { NextFunction, Request, Response } from 'express';
import * as coursesService from './courses.service';
import { ValidatedRequest } from 'express-joi-validation';
import {
  IAddLectorToCourseRequest,
  ICourseCreateRequest,
} from './types/course-create-request.interface';
import { ICourseUpdateRequest } from './types/course-update-request.interface';
import HttpStatuses from '../application/enums/http-statuses.enum';

export const getAllCourses = async (request: Request, response: Response) => {
  let courses;
  const { lector_id } = request.query;
  console.log(lector_id);
  if (lector_id) {
    courses = await coursesService.getCoursesByLectorId(lector_id as string);
  } else {
    courses = await coursesService.getAllCourses();
  }
  response.status(HttpStatuses.OK).json(courses);
};

export const getCourseById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const course = await coursesService.getCourseById(id);
  response.status(HttpStatuses.OK).json(course);
};

export const getMarksForCourse = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const courses = await coursesService.getMarksForCourse(id);
  if (!courses.length) {
    response
      .status(HttpStatuses.NOT_FOUND)
      .json({ error: 'Course is not found' });
  }
  response.status(HttpStatuses.OK).json(courses);
};

export const createCourse = async (
  request: ValidatedRequest<ICourseCreateRequest>,
  response: Response,
) => {
  const course = await coursesService.createCourse(request.body);
  response.status(HttpStatuses.CREATED).json(course);
};

export const addLectorToCourse = async (
  request: ValidatedRequest<IAddLectorToCourseRequest>,
  response: Response,
) => {
  const { courseId, lectorId } = request.params;
  const course = await coursesService.addLectorToCourse(courseId, lectorId);
  response.status(HttpStatuses.NO_CONTENT).json(course);
};

export const updateCourseById = async (
  request: ValidatedRequest<ICourseUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const course = await coursesService.updateCourseById(id, request.body);

  if (course) {
    response.status(HttpStatuses.OK).json(course);
  } else {
    response
      .status(HttpStatuses.NOT_FOUND)
      .json({ error: 'Course is not found' });
  }
};

export const deleteCourseById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const course = await coursesService.deleteCourseById(id);

  if (course) {
    response.status(HttpStatuses.OK).json(course);
  } else {
    response
      .status(HttpStatuses.NOT_FOUND)
      .json({ error: 'Course is not found' });
  }
};
