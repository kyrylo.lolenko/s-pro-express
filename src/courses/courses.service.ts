import HttpStatuses from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { ICourse } from './types/course.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Course } from './entities/course.entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { LectorCourse } from '../lector_course/entities/lector_course.entity';
import { getLectorById } from '../lectors/lectors.service';
import { Mark } from '../marks/entities/mark.entity';
import { Lector } from '../lectors/entities/lector.entity';

const coursesRepository = AppDataSource.getRepository(Course);

export const getAllCourses = async () => {
  return await coursesRepository.find({ relations: ['lectors', 'marks'] });
};

export const getCourseById = async (courseId: string) => {
  const course = await coursesRepository.findOne({
    where: {
      id: courseId,
    },
    relations: ['lectors', 'marks'],
  });

  if (!course) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }
  return course;
};

export const getMarksForCourse = async (courseId: string) => {
  const marksRepository = AppDataSource.getRepository(Mark);
  const marks = await marksRepository
    .createQueryBuilder('mark')
    .leftJoin('mark.courseId', 'course')
    .leftJoin('mark.lectorId', 'lector')
    .leftJoin('mark.studentId', 'student')
    .where('course.id = :courseId', { courseId })
    .select([
      'course.name as courseName',
      'lector.name as lectorName',
      'student.name as studentName',
      'mark.mark as mark',
    ])
    .getRawMany();

  return marks;
};

// export const getCoursesByLectorId = async (
//   lectorId: string,
// ): Promise<Course[]> => {
//   const courses = await coursesRepository.find({
//     where: { lectors: { id: lectorId } },
//     relations: ['lectors', 'marks'],
//   });

//   return courses;
// };

export const getCoursesByLectorId = async (
  lectorId: string,
): Promise<Course[]> => {
  const lector = await Lector.findOne({
    where: {
      id: lectorId,
    },
    relations: ['courses'],
  });

  if (!lector) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'There is no such lector',
    );
  }

  if (!lector.courses) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'The lector does not have any courses',
    );
  }

  return lector.courses;
};

export const createCourse = async (
  courseCreateSchema: Omit<ICourse, 'id'>,
): Promise<Course> => {
  const course = await coursesRepository.findOne({
    where: {
      name: courseCreateSchema.name,
    },
  });
  if (course) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Course with this name already exists',
    );
  }
  return coursesRepository.save(courseCreateSchema);
};

export const addLectorToCourse = async (
  courseId: string,
  lectorId: string,
): Promise<Course> => {
  const lectorsRepository = AppDataSource.getRepository(Lector);
  const course = await coursesRepository.findOne({
    where: {
      id: courseId,
    },
  });
  const lector = await lectorsRepository.findOne({
    where: {
      id: lectorId,
    },
  });

  if (!course || !lector) {
    throw new HttpException(HttpStatuses.BAD_REQUEST, 'There is no such id');
  }

  if (!course.lectors) {
    course.lectors = [];
  }

  if (!lector.courses) {
    lector.courses = [];
  }

  course.lectors.push(lector);
  lector.courses.push(course);

  await course.save();
  await lector.save();

  return course;
};

export const updateCourseById = async (
  id: string,
  courseUpdateSchema: Partial<ICourse>,
): Promise<UpdateResult> => {
  const result = await coursesRepository.update(id, courseUpdateSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Course with this id is not found',
    );
  }
  return result;
};

export const deleteCourseById = async (id: string): Promise<DeleteResult> => {
  const result = await coursesRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Course with this id is not found',
    );
  }
  return result;
};
