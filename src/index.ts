import 'dotenv/config';
import app, { appDataSourceInitialize } from './application/app';

const port = process.env.SERVER_PORT;

const startServer = async () => {
  app.listen(port, () => {
    appDataSourceInitialize()
      .then(() => {
        console.log('Typeorm connected to Database');
      })
      .catch((error) => {
        console.log(`Error: ${error}`);
      });
    console.log(`Server started at port ${port}`);
  });
};

startServer();
