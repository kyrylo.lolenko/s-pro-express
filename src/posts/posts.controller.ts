import { Response, Request } from 'express';
import { ValidatedRequest } from 'express-joi-validation';
import * as postsService from './posts.service';
import { IPostCreateRequest } from './types/post.create-request-interface';
import HttpStatuses from '../application/enums/http-statuses.enum';
import { FindOneOptions } from 'typeorm';
import { Post } from './entities/post.entity';
import { IPostUpdateRequest } from './types/post-update-request.interface';

export const getAllPosts = async (request: Request, response: Response) => {
  const posts = await postsService.getAllPosts();
  response.json(posts);
};

export const getPostById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const options: FindOneOptions<Post> = {
    where: { id },
  };
  const post = await postsService.getPostById(options);
  if (!post) {
    response.status(HttpStatuses.NOT_FOUND).json({ error: 'Post not found' });
  } else {
    response.json(post);
  }
};

export const createPost = async (
  request: ValidatedRequest<IPostCreateRequest>,
  response: Response,
) => {
  const post = await postsService.createPost(request.body);
  response.status(HttpStatuses.CREATED).json(post);
};

export const updatePostById = async (
  request: ValidatedRequest<IPostUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const { title, description } = request.body;

  const updatedPost = await postsService.updatePostById(id, {
    title,
    description,
  });

  if (updatedPost) {
    response.status(HttpStatuses.NO_CONTENT).json(updatedPost);
  } else {
    response.status(HttpStatuses.NOT_FOUND).json({ error: 'Post not found' });
  }
};

export const deletePostById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const result = await postsService.deletePostById(id);
  if (result) {
    response
      .status(HttpStatuses.NO_CONTENT)
      .json({ message: 'Post deleted successfully' });
  } else {
    response.status(HttpStatuses.NOT_FOUND).json({ error: 'Post not found' });
  }
};
