import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Post } from '../entities/post.entity';

export interface IPostCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Post, 'id'>;
}
