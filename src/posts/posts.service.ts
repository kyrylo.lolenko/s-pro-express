import HttpStatuses from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { Post } from './entities/post.entity';
import { IPost } from './types/post.interface';
import { FindOneOptions, UpdateResult, DeleteResult } from 'typeorm';

const postsRepository = AppDataSource.getRepository(Post);

export const getAllPosts = async (): Promise<Post[]> => {
  return await postsRepository.find({
    order: {
      createdAt: 'DESC',
    },
  });
};

export const getPostById = async (
  options: FindOneOptions<Post>,
): Promise<Post | null> => {
  return postsRepository.findOne(options);
};

export const createPost = async (
  createPostSchema: Omit<IPost, 'id'>,
): Promise<Post> => {
  return postsRepository.save({
    title: createPostSchema.title,
    description: createPostSchema.description,
  });
};

export const updatePostById = async (
  id: string,
  postUpdateSchema: Partial<IPost>,
): Promise<UpdateResult> => {
  const post = await postsRepository.update(id, postUpdateSchema);

  if (!post.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Student with this id is not found',
    );
  }
  return post;
};

export const deletePostById = async (id: string): Promise<DeleteResult> => {
  const result: DeleteResult = await postsRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Post not found');
  }
  return result;
};
