import HttpStatuses from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { AppDataSource } from '../configs/database/data-source';
import { Post } from './entities/post.entity';
import { createPost, deletePostById } from './posts.service';
import { DeleteResult } from 'typeorm';

const postsRepository = AppDataSource.getRepository(Post);

describe('post.service', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });

  describe('#createPost', () => {
    it('calls postsRepository.save with correct params', async () => {
      const spy = jest
        .spyOn(postsRepository, 'save')
        .mockImplementation((() => Promise.resolve()) as any);

      const title = 'Example';
      const description = 'Content';

      await createPost({ title, description });

      expect(spy.mock.calls).toEqual([[{ title, description }]]);
    });

    it('returns result of postsRepository.save ', async () => {
      const result = {};

      jest
        .spyOn(postsRepository, 'save')
        .mockImplementation((() => Promise.resolve({})) as any);

      const title = 'Example';
      const description = 'Content';

      const actualResult = await createPost({ title, description });

      expect(actualResult).toEqual(result);
    });

    it('spy leak test', async () => {
      const spy = jest
        .spyOn(postsRepository, 'save')
        .mockImplementation((() => console.log('Spy from first test')) as any);

      await createPost({} as any);

      spy.mockRestore();
    });
  });

  describe('#deletePostById', () => {
    it('deletes the post by ID', async () => {
      const mockDeleteResult: DeleteResult = {
        affected: 1,
        raw: {},
      };
      const mockPostId = '1';

      const deleteSpy = jest
        .spyOn(postsRepository, 'delete')
        .mockResolvedValue(mockDeleteResult);

      const result = await deletePostById(mockPostId);

      expect(deleteSpy).toHaveBeenCalledWith(mockPostId);
      expect(result).toEqual(mockDeleteResult);
    });

    it('throws HttpException when post is not found', async () => {
      const mockDeleteResult: DeleteResult = {
        affected: 0,
        raw: {},
      };
      const mockPostId = 'randomId';

      const deleteSpy = jest
        .spyOn(postsRepository, 'delete')
        .mockResolvedValue(mockDeleteResult);

      await expect(deletePostById(mockPostId)).rejects.toThrow(
        new HttpException(HttpStatuses.NOT_FOUND, 'Post not found'),
      );

      expect(deleteSpy).toHaveBeenCalledWith(mockPostId);
    });
  });
});
