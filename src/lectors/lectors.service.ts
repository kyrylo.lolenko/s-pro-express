import HttpStatuses from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { ILector } from './types/lector.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Lector } from './entities/lector.entity';
import { DeleteResult, UpdateResult } from 'typeorm';
import { Course } from '../courses/entities/course.entity';

const lectorsRepository = AppDataSource.getRepository(Lector);

export const getAllLectors = async (): Promise<Lector[]> => {
  return await lectorsRepository.find({});
};

export const getLectorById = async (
  id: string,
): Promise<Omit<ILector, 'courses'> & { courses: Course[] }> => {
  const lector = await lectorsRepository
    .createQueryBuilder('lector')
    .leftJoinAndSelect('lector.courses', 'courses')
    .where('lector.id = :id', { id })
    .getOne();

  if (!lector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Lector not found');
  }

  const courses = lector.courses || [];

  return {
    ...lector,
    courses,
  };
};

export const createLector = async (
  lectorCreateSchema: Omit<ILector, 'id'>,
): Promise<Lector> => {
  const lector = await lectorsRepository.findOne({
    where: {
      name: lectorCreateSchema.name,
    },
  });
  if (lector) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Lector with this name already exists',
    );
  }
  return lectorsRepository.save(lectorCreateSchema);
};

export const updateLectorById = async (
  id: string,
  lectorUpdateSchema: Partial<ILector>,
): Promise<UpdateResult> => {
  const result = await lectorsRepository.update(id, lectorUpdateSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Lector with this id is not found',
    );
  }
  return result;
};

export const deleteLectorById = async (id: string): Promise<DeleteResult> => {
  const result = await lectorsRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Lector with this id is not found',
    );
  }
  return result;
};
