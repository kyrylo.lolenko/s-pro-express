import { NextFunction, Request, Response } from 'express';
import * as lectorsService from './lectors.service';
import { ValidatedRequest } from 'express-joi-validation';
import { ILectorCreateRequest } from './types/lector-create-request.interface';
import { ILectorUpdateRequest } from './types/lector-update-request.interface';
import HttpStatuses from '../application/enums/http-statuses.enum';

export const getAllLectors = async (request: Request, response: Response) => {
  const lectors = await lectorsService.getAllLectors();
  response.status(HttpStatuses.OK).json(lectors);
};

export const getLectorById = async (
  request: Request<{ id: string }>,
  response: Response,
  next: NextFunction,
) => {
  const { id } = request.params;
  const lector = await lectorsService.getLectorById(id);
  response.json(lector);
};

export const createLector = async (
  request: ValidatedRequest<ILectorCreateRequest>,
  response: Response,
) => {
  const lector = await lectorsService.createLector(request.body);
  response.status(HttpStatuses.CREATED).json(lector);
};

export const updateLectorById = async (
  request: ValidatedRequest<ILectorUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorsService.updateLectorById(id, request.body);

  if (lector) {
    response.status(HttpStatuses.OK).json(lector);
  } else {
    response.status(HttpStatuses.NOT_FOUND).json({ error: 'Lector not found' });
  }
};

export const deleteLectorById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const lector = await lectorsService.deleteLectorById(id);

  if (lector) {
    response.status(HttpStatuses.OK).json(lector);
  } else {
    response.status(HttpStatuses.NOT_FOUND).json({ error: 'Lector not found' });
  }
};
