import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ILector } from './lector.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     LectorCreate:
 *       type: object
 *       required:
 *         - name
 *         - email
 *         - password
 *       properties:
 *         name:
 *           type: string
 *           description: name of lector
 *         email:
 *           type: string
 *           description: The lector email
 *         password:
 *           type: string
 *           description: password of lector
 *       example:
 *         name: Huzhva
 *         email: lector_email@gmail.com
 *         password: secretPass123
 */

export interface ILectorCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<ILector, 'id'>;
}
