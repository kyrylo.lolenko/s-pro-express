import { ILector } from './lector.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     Lector:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - email
 *         - password
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the lector
 *         createdAt:
 *           type: string
 *           format: date
 *           description: created at time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updated at time
 *         name:
 *           type: string
 *           description: name of lector
 *         email:
 *           type: string
 *           description: The lector email
 *         password:
 *           type: string
 *           description: Password of lector
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Huzhva
 *         email: user_email@gmail.com
 *         password: secretpass123
 */

export interface ILectorGetResponse extends ILector {
  createdAt: Date;
  updatedAt: Date;
}
