import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { ILector } from './lector.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     LectorUpdate:
 *       type: object
 *       properties:
 *        name:
 *          type: string
 *        email:
 *          type: string
 *        password:
 *          type: string
 *       example:
 *         name: Hordiienko
 *         email: lector_example@email.com
 *         password: ultraSecretPass235
 */

export interface ILectorUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<ILector>;
}
