import { Router } from 'express';
import * as lectorsController from './lectors.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { lectorUpdateSchema, lectorCreateSchema } from './lector.schema';
import { idParamSchema } from '../application/schemas/id-param.schema';

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors:
 *   get:
 *     summary: Get all lectors
 *     tags: [Lectors]
 *     responses:
 *       200:
 *         description: The list of lectors.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Lector'
 *       500:
 *         description: Some server error
 *
 */

router.get('/', controllerWrapper(lectorsController.getAllLectors));

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors/{id}:
 *   get:
 *     summary: Get lector by id
 *     tags: [Lectors]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *          type: string
 *         required: true
 *         description: Lector id
 *     responses:
 *       200:
 *         description: The lector by id.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Lector'
 *       404:
 *         description: The Lector was not found
 *       500:
 *         description: Some server error
 *
 */

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(lectorsController.getLectorById),
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors:
 *   post:
 *     summary: Create lector
 *     tags: [Lectors]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/LectorCreate'
 *     responses:
 *       201:
 *         description: Return the lector after creation.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Lector'
 *       400:
 *         description: Validation failed
 *       500:
 *         description: Some server error
 */

router.post(
  '/',
  validator.body(lectorCreateSchema),
  controllerWrapper(lectorsController.createLector),
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors/{id}:
 *   patch:
 *     summary: Update lector by id
 *     tags: [Lectors]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Lector id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/LectorUpdate'
 *     responses:
 *       204:
 *         description: The Lector was updated
 *       404:
 *         description: The Lector was not found
 *       500:
 *         description: Some server error
 */

router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(lectorUpdateSchema),
  controllerWrapper(lectorsController.updateLectorById),
);

/**
 * @swagger
 * tags:
 *   name: Lectors
 *   description: The lectors managing API
 * /lectors/{id}:
 *   delete:
 *     summary: Delete lector
 *     tags: [Lectors]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The lector id
 *     responses:
 *       200:
 *         description: lector was deleted
 *       404:
 *         description: lector was not found
 *       500:
 *         description: Some server error
 *
 */

router.delete('/:id', controllerWrapper(lectorsController.deleteLectorById));

export default router;
