/**
 * @swagger
 * components:
 *   schemas:
 *     Mark:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - mark
 *         - student
 *         - lector
 *         - course
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the mark
 *         createdAt:
 *           type: string
 *           format: date
 *           description: created at time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updated at time
 *         mark:
 *           type: string
 *           description: provided mark
 *         student:
 *           $ref: '#/components/schemas/Student'
 *           description: student which has this mark
 *         lector:
 *           $ref: '#/components/schemas/Lector'
 *           description: Lector which set this mark
 *         course:
 *           $ref: '#/components/schemas/Course'
 *           description: Course this mark was set for
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         mark: 12
 *         student: {Student}
 *         lector: {Lector}
 *         course: {Course}
 */
