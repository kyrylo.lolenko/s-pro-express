import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';

import { Mark } from '../entities/mark.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     MarkCreate:
 *       type: object
 *       required:
 *         - mark
 *         - studentId
 *         - lectorId
 *         - courseId
 *       properties:
 *         mark:
 *           type: string
 *           description: mark for student
 *         studentId:
 *           type: string
 *           description: id of student
 *         lectorId:
 *           type: string
 *           description: id of lector
 *         courseId:
 *           type: number
 *           description: id of course
 *       example:
 *         mark: 12
 *         studentId: 2
 *         lectorId: 3
 *         courseId: 4
 */

export interface IMarkCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Mark, 'id'>;
}
