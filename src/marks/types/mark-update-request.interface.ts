import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Mark } from '../entities/mark.entity';

export interface IMarkUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<Mark>;
}
