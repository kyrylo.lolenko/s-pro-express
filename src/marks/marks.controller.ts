import { Request, Response } from 'express';
import * as markService from './marks.service';
import { ValidatedRequest } from 'express-joi-validation';
import { IMarkCreateRequest } from './types/mark-create-request.interface';
import HttpStatuses from '../application/enums/http-statuses.enum';

export const getAllMarks = async (request: Request, response: Response) => {
  const marks = await markService.getAllMarks();
  response.status(HttpStatuses.OK).json(marks);
};

export const addMark = async (
  request: ValidatedRequest<IMarkCreateRequest>,
  response: Response,
) => {
  const { mark, courseId, studentId, lectorId } = request.body;
  const markData = { mark, courseId, studentId, lectorId };
  const markEntity = await markService.addMark(markData);
  response.json(markEntity);
};

export const deleteMark = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const markEntity = await markService.deleteMark(id);
  response.json(markEntity);
};
