import { Mark } from './entities/mark.entity';
import { Student } from '../students/entities/student.entity';
import { Course } from '../courses/entities/course.entity';
import { Lector } from '../lectors/entities/lector.entity';
import { AppDataSource } from '../configs/database/data-source';
import HttpException from '../application/exceptions/http-exception';
import HttpStatuses from '../application/enums/http-statuses.enum';
import { DeleteResult } from 'typeorm';

const marksRepository = AppDataSource.getRepository(Mark);

export async function getAllMarks(): Promise<Mark[]> {
  const marks = await marksRepository.find({
    relations: ['studentId', 'lectorId', 'courseId'],
  });

  return marks;
}
export const addMark = async (
  markCreateSchema: Partial<Mark>,
): Promise<void> => {
  const { mark, courseId, studentId, lectorId } = markCreateSchema;

  const isCourse = await AppDataSource.getRepository(Course).find({
    where: { id: `${courseId}` },
  });
  const isStudent = await AppDataSource.getRepository(Student).find({
    where: { id: `${studentId}` },
  });
  const isLector = await AppDataSource.getRepository(Lector).find({
    where: { id: `${lectorId}` },
  });

  if (!isCourse) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Course not found');
  }
  if (!isStudent) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'student not found');
  }
  if (!isLector) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'lector not found');
  }
  await marksRepository
    .createQueryBuilder()
    .insert()
    .into(Mark)
    .values({
      mark: mark,
      studentId,
      lectorId,
      courseId,
    })
    .execute();
};

export const deleteMark = async (id: string): Promise<DeleteResult> => {
  const mark = await marksRepository.delete(id);

  if (!mark.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return mark;
};
