import express from 'express';
import * as marksController from './marks.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { marksUpdateGroupSchema } from './mark.schema';
import { idParamSchema } from '../application/schemas/id-param.schema';

const router = express.Router();

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks:
 *   get:
 *     summary: Get all marks
 *     tags: [Marks]
 *     responses:
 *       200:
 *         description: The list of marks.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Mark'
 *       500:
 *         description: Some server error
 *
 */

router.get('/', controllerWrapper(marksController.getAllMarks));

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks:
 *   post:
 *     summary: Create mark
 *     tags: [Marks]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/MarkCreate'
 *     responses:
 *       200:
 *         description: Return the mark after creation.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Mark'
 *       400:
 *         description: Validation failed
 *       500:
 *         description: Some server error
 */

router.post(
  '/',
  validator.body(marksUpdateGroupSchema),
  controllerWrapper(marksController.addMark),
);

/**
 * @swagger
 * tags:
 *   name: Marks
 *   description: The marks managing API
 * /marks/{id}:
 *   delete:
 *     summary: Delete mark
 *     tags: [Marks]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The mark id
 *     responses:
 *       200:
 *         description: Mark was deleted
 *       404:
 *         description: Mark was not found
 *       500:
 *         description: Some server error
 *
 */

router.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(marksController.deleteMark),
);

export default router;
