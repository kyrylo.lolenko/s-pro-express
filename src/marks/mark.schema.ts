import Joi from 'joi';
import { Mark } from './entities/mark.entity';

export const marksUpdateGroupSchema = Joi.object<Partial<Mark>>({
  mark: Joi.number().required(),
  studentId: Joi.number().required(),
  lectorId: Joi.number().required(),
  courseId: Joi.number().required(),
});
