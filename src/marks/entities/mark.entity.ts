import {
  Column,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Student } from '../../students/entities/student.entity';
import { Lector } from '../../lectors/entities/lector.entity';
import { Course } from '../../courses/entities/course.entity';

@Entity({ name: 'marks' })
export class Mark extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  mark: string;

  @ManyToOne(() => Student, (student) => student.marks)
  studentId: Student;

  @ManyToOne(() => Lector, (lector) => lector.marks)
  lectorId: Lector;

  @ManyToOne(() => Course, (course) => course.marks)
  courseId: Course;
}
