import { Router } from 'express';
import * as studentsController from './students.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import {
  studentCreateSchema,
  studentUpdateGroupSchema,
  studentUpdateSchema,
} from './student.schema';
import { idParamSchema } from '../application/schemas/id-param.schema';

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students:
 *   get:
 *     summary: Get all students
 *     tags: [Students]
 *     responses:
 *       200:
 *         description: The list of students.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Student'
 *       500:
 *         description: Some server error
 *
 */

router.get('/', controllerWrapper(studentsController.getAllStudents));

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *   get:
 *     summary: Get student by id
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *          type: string
 *         required: true
 *         description: Student id
 *     responses:
 *       200:
 *         description: Student by id.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Student'
 *       404:
 *         description: Student was not found
 *       500:
 *         description: Some server error
 *
 */

router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getStudentById),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}/marks:
 *   get:
 *     summary: Get student by id
 *     tags: [Students]
 *     parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: Student id
 *     responses:
 *       200:
 *         description: Student by id with marks.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/StudentsWithMarks'
 *       404:
 *         description: Student was not found
 *       500:
 *         description: Some server error
 */

router.get(
  '/:id/marks',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.getStudentMarks),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students:
 *   post:
 *     summary: Create student
 *     tags: [Students]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/StudentCreate'
 *     responses:
 *       200:
 *         description: Return the student after creation.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Student'
 *       400:
 *         description: Validation failed
 *       500:
 *         description: Some server error
 */

router.post(
  '/',
  validator.body(studentCreateSchema),
  controllerWrapper(studentsController.createStudent),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *   patch:
 *     summary: Update student by id
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Student id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/StudentUpdate'
 *     responses:
 *       204:
 *         description: Student was updated
 *       404:
 *         description: Student was not found
 *       500:
 *         description: Some server error
 */

router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(studentUpdateSchema),
  controllerWrapper(studentsController.updateStudentById),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}/group:
 *   patch:
 *     summary: Add group to selected student
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Student id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/StudentUpdateGroup'
 *     responses:
 *       204:
 *         description: Student was updated
 *       404:
 *         description: Student was not found
 */

router.patch(
  '/:id/group',
  validator.params(idParamSchema),
  validator.body(studentUpdateGroupSchema),
  controllerWrapper(studentsController.addGroupToStudent),
);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}/image:
 *   patch:
 *     summary: Update student's image path
 *     tags: [Students]
 *     parameters:
 *       - name: id
 *         in: path
 *         description: ID of the student to update
 *         required: true
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               imagePath:
 *                 type: string
 *     responses:
 *       204:
 *         description: Student was updated
 *       404:
 *         description: Student was not found
 *       500:
 *         description: Some server error
 */

router.patch('/:id/image', studentsController.addImage);

/**
 * @swagger
 * tags:
 *   name: Students
 *   description: The students managing API
 * /students/{id}:
 *   delete:
 *     summary: Delete student
 *     tags: [Students]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The student id
 *     responses:
 *       200:
 *         description: The student was deleted
 *       404:
 *         description: The student was not found
 *       500:
 *         description: Some server error
 *
 */

router.delete(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(studentsController.deleteStudentById),
);

export default router;
