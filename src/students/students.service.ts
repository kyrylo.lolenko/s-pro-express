import HttpStatuses from '../application/enums/http-statuses.enum';
import HttpException from '../application/exceptions/http-exception';
import { IStudent } from './types/student.interface';
import { AppDataSource } from '../configs/database/data-source';
import { Student } from './entities/student.entity';
import {
  DeleteResult,
  FindManyOptions,
  Like,
  Repository,
  UpdateResult,
} from 'typeorm';

const studentsRepository = AppDataSource.getRepository(Student);

export const getAllStudents = async (name?: string): Promise<Student[]> => {
  const options: FindManyOptions<Student> = {};

  if (name) {
    options.where = { name: Like(`%${name}%`) };
  }

  const students = await studentsRepository.find(options);

  if (!name) {
    return students;
  }

  return students.filter((student) =>
    student.name.toLowerCase().includes(name.toLowerCase()),
  );
};

export const getStudentById = async (
  id: string,
): Promise<Omit<Student, 'groupId'> & { groupName: string }> => {
  const student = await studentsRepository
    .createQueryBuilder('student')
    .select([
      'student.id as id',
      'student.name as name',
      'student.surname as surname',
      'student.email as email',
      'student.age as age',
      'student.imagePath as imagePath',
    ])
    .leftJoin('student.group', 'group')
    .addSelect('group.name as groupName')
    .where('student.id = :id', { id })
    .getRawOne();
  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }
  return student;
};

export const getStudentMarks = async (id: string): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      id: id,
    },
    relations: ['marks', 'marks.courseId'],
  });

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  return student;
};

export const createStudent = async (
  studentCreateSchema: Omit<IStudent, 'id'>,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      email: studentCreateSchema.email,
    },
  });
  if (student) {
    throw new HttpException(
      HttpStatuses.BAD_REQUEST,
      'Student with this email already exists',
    );
  }
  return studentsRepository.save(studentCreateSchema);
};

export const updateStudentById = async (
  id: string,
  studentUpdateSchema: Partial<IStudent>,
): Promise<UpdateResult> => {
  const result = await studentsRepository.update(id, studentUpdateSchema);

  if (!result.affected) {
    throw new HttpException(
      HttpStatuses.NOT_FOUND,
      'Student with this id is not found',
    );
  }
  return result;
};

export const addGroupToStudent = async (
  studentId: string,
  groupId: number,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: { id: studentId },
  });

  if (!student) {
    throw new Error('Student not found');
  }

  student.groupId = groupId;
  await studentsRepository.save(student);

  return student;
};

export const addImage = async (
  studentId: string,
  imagePath: string,
): Promise<Student> => {
  const student = await studentsRepository.findOne({
    where: {
      id: studentId,
    },
  });

  if (!student) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }

  student.imagePath = imagePath;

  const updatedStudent = await studentsRepository.save(student);

  return updatedStudent;
};

export const deleteStudentById = async (id: string): Promise<DeleteResult> => {
  const result = await studentsRepository.delete(id);

  if (!result.affected) {
    throw new HttpException(HttpStatuses.NOT_FOUND, 'Student not found');
  }
  return result;
};
