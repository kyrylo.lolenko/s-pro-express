import {
  Column,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { CoreEntity } from '../../application/entities/core.entity';
import { Group } from '../../groups/entities/group.entity';
import { Mark } from '../../marks/entities/mark.entity';

@Entity({ name: 'students' })
export class Student extends CoreEntity {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  @Index('idx_students_name')
  name: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  surname: string;

  @Column({
    type: 'varchar',
    nullable: false,
  })
  email: string;

  @Column({
    type: 'numeric',
    nullable: true,
  })
  age: number;

  @Column({
    type: 'varchar',
    nullable: true,
  })
  imagePath: string;

  @ManyToOne(() => Group, (group) => group.students, {
    nullable: true,
    eager: false,
  })
  @JoinColumn({ name: 'group_id' })
  group: Group;

  @Column({
    type: 'integer',
    nullable: true,
    name: 'group_id',
  })
  groupId: number;

  @OneToMany(() => Mark, (mark) => mark.studentId)
  marks: Mark[];
}
