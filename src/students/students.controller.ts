import { NextFunction, Request, Response } from 'express';
import * as studentsService from './students.service';
import { ValidatedRequest } from 'express-joi-validation';
import {
  IAddGroupToStudentRequest,
  IStudentUpdateRequest,
} from './types/student-update-request.interface';
import { IStudentCreateRequest } from './types/student-create-request.interface';
import HttpStatuses from '../application/enums/http-statuses.enum';

export const getAllStudents = async (request: Request, response: Response) => {
  const { name } = request.query;
  const students = await studentsService.getAllStudents(name as string);

  response.status(HttpStatuses.OK).json(students);
};

export const getStudentById = async (
  request: Request<{ id: string }>,
  response: Response,
  next: NextFunction,
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentById(id);
  response.json(student);
};

export const getStudentMarks = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.getStudentMarks(id);
  response.json(student);
};

export const createStudent = async (
  request: ValidatedRequest<IStudentCreateRequest>,
  response: Response,
) => {
  const student = await studentsService.createStudent(request.body);
  response.status(HttpStatuses.CREATED).json(student);
};

export const updateStudentById = async (
  request: ValidatedRequest<IStudentUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.updateStudentById(id, request.body);

  if (student) {
    response.status(HttpStatuses.NO_CONTENT).json(student);
  } else {
    response
      .status(HttpStatuses.NOT_FOUND)
      .json({ error: 'Student not found' });
  }
};

export const addGroupToStudent = async (
  request: ValidatedRequest<IAddGroupToStudentRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const { group } = request.body;

  const student = await studentsService.addGroupToStudent(id, Number(group));
  response.json(student);
};

export const addImage = async (req: Request, res: Response): Promise<void> => {
  const { id } = req.params;
  const { imagePath } = req.body;

  const updatedStudent = await studentsService.addImage(id, imagePath);

  if (updatedStudent) {
    res.status(HttpStatuses.NO_CONTENT).json(updatedStudent);
  } else {
    res.status(HttpStatuses.NOT_FOUND).json({ error: 'Student not found' });
  }
};

export const deleteStudentById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const student = await studentsService.deleteStudentById(id);

  if (student) {
    response.status(HttpStatuses.OK).json(student);
  } else {
    response
      .status(HttpStatuses.NOT_FOUND)
      .json({ error: 'Student not found' });
  }
};
