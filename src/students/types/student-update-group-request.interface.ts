/**
 * @swagger
 * components:
 *   schemas:
 *     StudentUpdateGroup:
 *       type: object
 *       properties:
 *        group:
 *          type: string
 *       example:
 *         group: 1
 */
