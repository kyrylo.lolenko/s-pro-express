import { IStudent } from './student.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     Student:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - surname
 *         - email
 *         - age
 *         - imagePath
 *         - groupId
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the student
 *         createdAt:
 *           type: string
 *           format: date
 *           description: created at time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updated at time
 *         name:
 *           type: string
 *           description: name of student
 *         surname:
 *           type: string
 *           description: surname of student
 *         email:
 *           type: string
 *           description: The student email
 *         age:
 *           type: number
 *           description: age of student
 *         imagePath:
 *           type: string
 *           description: The student's image path
 *         groupId:
 *           type: string
 *           description: id of group of student
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Ivan
 *         surname: Petrychenko
 *         email: user_email@gmail.com
 *         age: 25
 *         imagePath: pathToImage
 *         groupId: 1
 */

export interface IStudentGetResponse extends IStudent {
  createdAt: Date;
  updatedAt: Date;
}
