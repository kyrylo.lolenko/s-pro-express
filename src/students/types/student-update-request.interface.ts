import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IStudent } from './student.interface';
import { Student } from '../entities/student.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentUpdate:
 *       type: object
 *       properties:
 *        name:
 *          type: string
 *        surname:
 *          type: string
 *        age:
 *          type: number
 *        email:
 *          type: string
 *        imagePath:
 *          type: string
 *       example:
 *         email: example@email.com
 *         name: John
 *         surname: Doe
 *         age: 25
 *         imagePath: pathToImage
 */

export interface IStudentUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IStudent>;
}

export interface IAddGroupToStudentRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Pick<Student, 'group'>;
}
