import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Student } from '../entities/student.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     StudentCreate:
 *       type: object
 *       required:
 *         - name
 *         - surname
 *         - email
 *         - age
 *         - groupId
 *       properties:
 *         name:
 *           type: string
 *           description: name of student
 *         surname:
 *           type: string
 *           description: surname of student
 *         email:
 *           type: string
 *           description: The student email
 *         age:
 *           type: number
 *           description: age of student
 *         imagePath:
 *           type: string
 *           required: false
 *           description: path of student's image
 *         groupId:
 *           type: string
 *           required: false
 *           description: id of group of student
 *       example:
 *         name: Ivan
 *         surname: Petrychenko
 *         email: user_email@gmail.com
 *         age: 25
 *         imagePath: pathToImage
 *         groupId: 1
 */

export interface IStudentCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Student, 'id'>;
}
