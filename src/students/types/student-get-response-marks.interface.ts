/**
 * @swagger
 * components:
 *   schemas:
 *     StudentsWithMarks:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - surname
 *         - email
 *         - age
 *         - imagePath
 *         - groupId
 *         - marks
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the book
 *         createdAt:
 *           type: string
 *           format: date
 *           description: createdAt time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updatedAt time
 *         name:
 *           type: string
 *           description: student name
 *         surname:
 *           type: string
 *           description: student surname
 *         email:
 *           type: string
 *           description: student email
 *         age:
 *           type: number
 *           description: student age
 *         imagePath:
 *           type: ['string','null']
 *           description: student imagePath
 *         groupId:
 *           type: string
 *           description: id of group of student
 *         marks:
 *           type: ['array', 'null']
 *           description: student marks
 *           items:
 *              $ref: '#/components/schemas/Mark'
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: Taras
 *         surname: Shevchenko
 *         email: student_mail@mail
 *         age: 25
 *         imagePath: path
 *         groupId: 3
 *         marks: []
 */
