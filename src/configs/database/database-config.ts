import { DataSourceOptions } from 'typeorm';
import path from 'path';

export const databaseConfiguration = (
  isMigrationRun = true,
): DataSourceOptions => {
  const ROOT_PATH: string = process.cwd();
  // const migrationsPath = `${ROOT_PATH}/build/**/migrations/*{.ts,.js}`;
  // const entitiesPath = `${ROOT_PATH}/build/**/*.entity{.ts,.js}`;
  // const migrationsPath = `${ROOT_PATH}/src/**/migrations/*{.ts,.js}`;
  // const entitiesPath = `${ROOT_PATH}/src/**/*.entity{.ts,.js}`;

  const migrationsPath = path.join(
    __dirname,
    '../../',
    '**/migrations/*{.ts,.js}',
  );
  const entitiesPath = path.join(__dirname, '../../', '**/*.entity{.ts,.js}');

  return {
    type: 'postgres',
    host: process.env.DATABASE_HOST,
    port: Number(process.env.DATABASE_PORT),
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME,
    entities: [entitiesPath],
    migrations: [migrationsPath],
    migrationsTableName: 'migrations',
    migrationsRun: isMigrationRun,
    logging: true,
    synchronize: false,
  };
};
