import { NextFunction, Request, Response } from 'express';
import * as groupsService from './groups.service';
import { IGroupCreateRequest } from './types/group-create-request.interface';
import { IGroupUpdateRequest } from './types/group-update-request.interface';
import { ValidatedRequest } from 'express-joi-validation';
import HttpStatuses from '../application/enums/http-statuses.enum';
import { Group } from './entities/group.entity';

export const getAllGroups = async (request: Request, response: Response) => {
  const groups = await groupsService.getAllGroupsWithStudents();
  response.status(HttpStatuses.OK).json(groups);
};

export const getGroupById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const groupWithStudents = await groupsService.getStudentsByGroupId(id);
  response.json(groupWithStudents);
};

export const createGroup = async (
  request: ValidatedRequest<IGroupCreateRequest>,
  response: Response,
) => {
  const group = await groupsService.createGroup(request.body);
  response.status(HttpStatuses.CREATED).json(group);
};

export const updateGroupById = async (
  request: ValidatedRequest<IGroupUpdateRequest>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.updateGroupById(id, request.body);
  response.json(group);
};

export const deleteGroupById = async (
  request: Request<{ id: string }>,
  response: Response,
) => {
  const { id } = request.params;
  const group = await groupsService.deleteGroupById(id);
  response.json(group);
};
