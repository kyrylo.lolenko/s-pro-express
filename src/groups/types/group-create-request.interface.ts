import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { Group } from '../entities/group.entity';

/**
 * @swagger
 * components:
 *   schemas:
 *     GroupCreate:
 *       type: object
 *       required:
 *         - name
 *       properties:
 *         name:
 *           type: string
 *           description: name of group
 *       example:
 *         name: IK-205
 */

export interface IGroupCreateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Omit<Group, 'id'>;
}
