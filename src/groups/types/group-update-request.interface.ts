import { ContainerTypes, ValidatedRequestSchema } from 'express-joi-validation';
import { IGroup } from './group.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     GroupUpdate:
 *       type: object
 *       properties:
 *        name:
 *          type: string
 *       example:
 *         name: Physics Group
 */

export interface IGroupUpdateRequest extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Partial<IGroup>;
}
