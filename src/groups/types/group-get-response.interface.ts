import { IGroup } from './group.interface';

/**
 * @swagger
 * components:
 *   schemas:
 *     Group:
 *       type: object
 *       required:
 *         - id
 *         - createdAt
 *         - updatedAt
 *         - name
 *         - students
 *       properties:
 *         id:
 *           type: string
 *           description: The auto-generated id of the student
 *         createdAt:
 *           type: string
 *           format: date
 *           description: created at time
 *         updatedAt:
 *           type: string
 *           format: date
 *           description: updated at time
 *         name:
 *           type: string
 *           description: Group name
 *         students:
 *           type: ['array', 'null']
 *           description: Students in this group
 *           items:
 *              $ref: '#/components/schemas/Student'
 *       example:
 *         id: 1
 *         createdAt: 2020-03-10T04:05:06.157Z
 *         updatedAt: 2020-03-10T04:05:06.157Z
 *         name: IH-403K group
 *         students: []
 */

export interface IGroupGetResponse extends IGroup {
  createdAt: Date;
  updatedAt: Date;
}
