import { Router } from 'express';
import * as groupsController from './groups.controller';
import controllerWrapper from '../application/utilities/controller-wrapper';
import validator from '../application/middlewares/validation.middleware';
import { groupUpdateSchema, groupCreateSchema } from './group.schema';
import { idParamSchema } from '../application/schemas/id-param.schema';

const router = Router();

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups:
 *   get:
 *     summary: Get all groups
 *     tags: [Groups]
 *     responses:
 *       200:
 *         description: The list of groups.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                $ref: '#/components/schemas/Group'
 *       500:
 *         description: Some server error
 *
 */

router.get('/', controllerWrapper(groupsController.getAllGroups));

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups/{id}:
 *   get:
 *     summary: Get group by id
 *     tags: [Groups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *          type: string
 *         required: true
 *         description: Group id
 *     responses:
 *       200:
 *         description: Group by id.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Group'
 *       404:
 *         description: Group was not found
 *       500:
 *         description: Some server error
 *
 */
router.get(
  '/:id',
  validator.params(idParamSchema),
  controllerWrapper(groupsController.getGroupById),
);

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups:
 *   post:
 *     summary: Create group
 *     tags: [Groups]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/GroupCreate'
 *     responses:
 *       200:
 *         description: Return the group after creation.
 *         content:
 *           application/json:
 *             schema:
 *                $ref: '#/components/schemas/Group'
 *       400:
 *         description: Validation failed
 *       500:
 *         description: Some server error
 */

router.post(
  '/',
  validator.body(groupCreateSchema),
  controllerWrapper(groupsController.createGroup),
);

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups/{id}:
 *   patch:
 *     summary: Update group by id
 *     tags: [Groups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: Group id
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *            $ref: '#/components/schemas/GroupUpdate'
 *     responses:
 *       204:
 *         description: Group was updated
 *       404:
 *         description: Group was not found
 *       500:
 *         description: Some server error
 */

router.patch(
  '/:id',
  validator.params(idParamSchema),
  validator.body(groupUpdateSchema),
  controllerWrapper(groupsController.updateGroupById),
);

/**
 * @swagger
 * tags:
 *   name: Groups
 *   description: The groups managing API
 * /groups/{id}:
 *   delete:
 *     summary: Delete group
 *     tags: [Groups]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The group id
 *     responses:
 *       200:
 *         description: Group was deleted
 *       404:
 *         description: Group was not found
 *       500:
 *         description: Some server error
 *
 */

router.delete('/:id', controllerWrapper(groupsController.deleteGroupById));

export default router;
