import { MigrationInterface, QueryRunner } from "typeorm";

export class ChangeContentFieldToDescriptionField1689626905751 implements MigrationInterface {
    name = 'ChangeContentFieldToDescriptionField1689626905751'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "posts" RENAME COLUMN "content" TO "description"`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "posts" RENAME COLUMN "description" TO "content"`);
    }

}
