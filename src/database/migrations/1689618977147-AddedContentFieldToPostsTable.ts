import { MigrationInterface, QueryRunner } from "typeorm";

export class AddedContentFieldToPostsTable1689618977147 implements MigrationInterface {
    name = 'AddedContentFieldToPostsTable1689618977147'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "posts" ADD "content" character varying NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "posts" DROP COLUMN "content"`);
    }

}
