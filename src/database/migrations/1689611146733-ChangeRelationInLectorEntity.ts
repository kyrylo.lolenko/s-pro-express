import { MigrationInterface, QueryRunner } from "typeorm";

export class ChangeRelationInLectorEntity1689611146733 implements MigrationInterface {
    name = 'ChangeRelationInLectorEntity1689611146733'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP INDEX "public"."IDX_fa21194644d188132582b0d1a3"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_67ca379415454fe43871951552"`);
        await queryRunner.query(`CREATE TABLE "lectors_courses_courses" ("lectorsId" integer NOT NULL, "coursesId" integer NOT NULL, CONSTRAINT "PK_93b042524c965b1d04d333edc21" PRIMARY KEY ("lectorsId", "coursesId"))`);
        await queryRunner.query(`CREATE INDEX "IDX_5f00c708e79b6580a23fac84a6" ON "lectors_courses_courses" ("lectorsId") `);
        await queryRunner.query(`CREATE INDEX "IDX_ee8c77e075e5f81bc349fd9743" ON "lectors_courses_courses" ("coursesId") `);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "PK_79cf0f064235769277ce94c75f7"`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "PK_a258ae675f3378b94898c00a148" PRIMARY KEY ("id", "lector_id", "course_id")`);
        await queryRunner.query(`ALTER TABLE "lectors_courses_courses" ADD CONSTRAINT "FK_5f00c708e79b6580a23fac84a6d" FOREIGN KEY ("lectorsId") REFERENCES "lectors"("id") ON DELETE CASCADE ON UPDATE CASCADE`);
        await queryRunner.query(`ALTER TABLE "lectors_courses_courses" ADD CONSTRAINT "FK_ee8c77e075e5f81bc349fd9743e" FOREIGN KEY ("coursesId") REFERENCES "courses"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "lectors_courses_courses" DROP CONSTRAINT "FK_ee8c77e075e5f81bc349fd9743e"`);
        await queryRunner.query(`ALTER TABLE "lectors_courses_courses" DROP CONSTRAINT "FK_5f00c708e79b6580a23fac84a6d"`);
        await queryRunner.query(`ALTER TABLE "lector_course" DROP CONSTRAINT "PK_a258ae675f3378b94898c00a148"`);
        await queryRunner.query(`ALTER TABLE "lector_course" ADD CONSTRAINT "PK_79cf0f064235769277ce94c75f7" PRIMARY KEY ("lector_id", "course_id")`);
        await queryRunner.query(`DROP INDEX "public"."IDX_ee8c77e075e5f81bc349fd9743"`);
        await queryRunner.query(`DROP INDEX "public"."IDX_5f00c708e79b6580a23fac84a6"`);
        await queryRunner.query(`DROP TABLE "lectors_courses_courses"`);
        await queryRunner.query(`CREATE INDEX "IDX_67ca379415454fe43871951552" ON "lector_course" ("course_id") `);
        await queryRunner.query(`CREATE INDEX "IDX_fa21194644d188132582b0d1a3" ON "lector_course" ("lector_id") `);
    }

}
