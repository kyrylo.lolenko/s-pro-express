import express from 'express';
import cors from 'cors';
import logger from './middlewares/logger.middleware';
import studentsRouter from '../students/students.router';
import groupsRouter from '../groups/groups.router';
import coursesRouter from '../courses/courses.router';
import lectorsRouter from '../lectors/lectors.router';
import marksRouter from '../marks/marks.router';
import postsRouter from '../posts/posts.router';
import bodyParser from 'body-parser';
import exceptionsFilter from './middlewares/exceptions.filter';
import path from 'path';
import { AppDataSource } from '../configs/database/data-source';
import swaggerJsdoc from 'swagger-jsdoc';
import swaggerUi from 'swagger-ui-express';

const app = express();

app.use(cors());
app.use(bodyParser.json());
app.use(logger);


const options = {
  definition: {
    openapi: '3.1.0',
    info: {
      title: 'University API',
      version: '0.1.0',
      description: 'University API',
      license: {
        name: 'MIT',
        url: 'https://spdx.org/licenses/MIT.html',
      },
    },
    servers: [
      {
        url: 'http://localhost:3001/api/v1',
      },
    ],
  },
  apis: ['./**/*.router.ts', './**/*.interface.ts'],
};

const specs = swaggerJsdoc(options);
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(specs));


export const appDataSourceInitialize = async () => {
  return await AppDataSource.initialize();
};


const staticFilesPath = path.join(__dirname, '../', 'public');

app.use('/api/v1/public', express.static(staticFilesPath));
app.use('/api/v1/students', studentsRouter);
app.use('/api/v1/groups', groupsRouter);
app.use('/api/v1/courses', coursesRouter);
app.use('/api/v1/lectors', lectorsRouter);
app.use('/api/v1/marks', marksRouter);
app.use('/api/v1/posts', postsRouter);

app.use(exceptionsFilter);

export default app;
