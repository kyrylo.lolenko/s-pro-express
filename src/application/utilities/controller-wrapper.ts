import { Request, Response, NextFunction } from 'express';

const controllerWrapper = (requestHandler: any) => {
  return async (request: Request, response: Response, next: NextFunction) => {
    try {
      await requestHandler(request, response, next);
    } catch (err) {
      next(err);
    }
  };
};

export default controllerWrapper;
